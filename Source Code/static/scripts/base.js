$(function(){
	
	var ratingDiv = $('#ratingDiv');
	var showRating = parseInt(ratingDiv.text());
	ratingDiv.text('');
	
	for(var i=0; i<showRating; i++){
		ratingDiv.append('<span class="glyphicon glyphicon-star"></span>');		
	}
	ratingDiv.append(' ('+showRating+')');
	
	$('#venueEdit').on('click', function(){
		
		var newVenueId = prompt("Please enter new venue ID:");
		if (newVenueId !== undefined && newVenueId !==  ""){
			var newId = parseInt(newVenueId);
			if(newId !== undefined && Number.isInteger(newId)){
				console.log(newId);
				
				$.ajax({
					url: "/update_venue_id",
					method: "put",
					data: {
						venueId: newId,
						showName: $('#showNameDiv').text()
					},
					success: function(){
						console.log("Venue ID successfully updated.");
						$('#venueIdHref').text(newId);				
					}
					
				});				
			} 		
		}	
	});
	
	
});