from flask import Flask, url_for, request, render_template, jsonify, session, flash, redirect
import json, os

app = Flask(__name__)

app.secret_key= 'A0Zr98j/3yXR~XHH!jmN]LWX/,?RT'


with app.open_resource('static/data/fringeshows.json') as file:
	data = json.load(file)


@app.route('/')
def home():
	if not session.get('logged_in'):
		return redirect(url_for('login'));	
	else:
		return render_template('home.html', user=session['username'] );
		
		
@app.route('/login', methods=['GET', 'POST'])
def login():
	if request.method == 'POST':
		if request.form['password'] == 'password' and request.form['username'] == 'Chris':
			session['logged_in'] = True;
			session['username'] = request.form['username'];
			return redirect(url_for('home'));
		else:
			flash('Incorrect Username/Password combo')
			return redirect(url_for('login'));
	else:
		return render_template('login.html');
		
@app.route('/update_venue_id', methods=['PUT'])
def update_venue_id():
	if not session.get('logged_in'):
		return redirect(url_for('login'));
	if request.method == 'PUT':
		if request.form['venueId'] and request.form['showName']:
			newVenueId = request.form['venueId'];
			showName = request.form['showName'];
			return update_venue(newVenueId, showName);
		else:
			return 'failure'
	else:
		return 'failure'
		
@app.route('/logout')
def logout():
	if session.get('logged_in'):
		session['logged_in'] = False;
		session.pop('username', None)
		return redirect(url_for('home'));
	return redirect(url_for('home'));
		

@app.route('/categories')
@app.route('/category/<genre>')
def get_category(genre = None):
	if not session.get('logged_in'):
		return redirect(url_for('login'));
	if genre is None:
		categoryList = []
		for show in data:
			if data[show]['category'] not in categoryList:
				categoryList.append(data[show]['category'])
		
		return render_template('venue.html', categoryList=categoryList, user=session['username']);	
	else:
		showCategoryList  = []
		for show in data:
			print show;
			if(data[show]['category'].lower() == genre):
				showCategoryList.append(data[show])
				
		if showCategoryList:
			return render_template('venue.html', showCategoryList=showCategoryList, user=session['username']);
		else:
			return render_template('error.html', user=session['username']);
		


@app.route('/venues')
@app.route('/venue/<int:id>')
def get_venue(id = None):
	if not session.get('logged_in'):
		return redirect(url_for('login'));
		
	if id is None:
		venueList = []	
		for show in data:
			if data[show]['venue'] not in venueList:
				venueList.append(data[show]['venue'])			
		venueList.sort()		
		return render_template('venue.html', venues=venueList, user=session['username']);
	else:
		showList  = []
		for show in data:
			if(int(data[show]['venue']) == id):
				showList.append(data[show])		
		showList.sort()
		if showList:
			return render_template('venue.html', showList=showList, user=session['username']);
		else:
			return render_template('error.html', user=session['username']);


@app.route('/shows')
@app.route('/show/<name>')
def get_show(name = None):
	if not session.get('logged_in'):
		return redirect(url_for('login'));
		
	if name is None:
		showList  = []
		for show in data:
			showList.append(data[show])
		return render_template('show.html', showList=showList, user=session['username']);
	else:
		showData  = []
		for show in data:
			if(data[show]['url'] == name):		
				showData.append(data[show])
		if showData:		
			return render_template('show.html', showData=showData, user=session['username']);
		else:
			return render_template('error.html', user=session['username']);



def update_venue(newVenueId, showName):
	for show in data:
		if(data[show]['name'] == showName):
			data[show]['venue'] = newVenueId;
			with open(os.path.join(app.root_path, 'static/data/fringeshows.json'), 'w') as file:
				json.dump(data, file)
				return 'success';
	return 'failure';	
		
	
@app.errorhandler(404)
def page_not_found(error):
	if not session.get('logged_in'):
		return redirect(url_for('login'));
	return render_template('error.html', user=session['username']);


if __name__ == "__main__":
	app.run(host='0.0.0.0' ,debug=True)
